/**
 * FileController
 *
 * @description :: Server-side logic for managing Files
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

   /**
    * Upload avatar for currently logged-in user
    *
    * (POST /user/avatar)
    */
   upload: function (req, res) {
      var id = req.param('id');
      console.log('fileType',req.param('fileType'));
      console.log('id',id);

      req.file('file').upload({
         dirname: './assets/images',
         // don't allow the total upload size to exceed ~50MB
         maxBytes: 50000000
      },function UploadFinished(err, uploadedFiles) {
         if (err) {
            return res.negotiate(err);
         }

         // If no files were uploaded, respond with an error.
         if (uploadedFiles.length === 0){
            return res.badRequest('No file was uploaded');
         }

         var url = require('util').format('%s/file/%s/%s', sails.getBaseUrl(),req.param('fileType'), id);

         var result;

         console.log(req.param('fileType'));
         if(req.param('fileType')==='logo') {
           result = EventService.updateLogo(id, url, uploadedFiles[0].fd);
         }else{
            result =  EventService.updateBackground(id, url, uploadedFiles[0].fd);
         }
         result.exec(function (err){
               if (err) return res.negotiate(err);
               return res.ok();

               /*return res.json({
                  message: ' Background img uploaded successfully!',
               });*/
            });
      });
   },

   /**
    * Download avatar of the user with the specified id
    *
    * (GET /event/background/:id)
    */
   background: function (req, res){

      console.log('Get background');

      console.log(req.param('id'));
      req.validate({
         id: 'string'
      });

      Event.findOne(req.param('id')).exec(function (err, event){
         console.log('findedOne')
         if (err) return res.negotiate(err);
         if (!event) return res.notFound();

         // Event has no background image uploaded.
         // (should have never have hit this endpoint and used the default image)
         if (!event.backgroundFd) {
            return res.notFound();
         }

         var SkipperDisk = require('skipper-disk');
         var fileAdapter = SkipperDisk(/* optional opts */);

         // Stream the file down
         fileAdapter.read(event.backgroundFd)
            .on('error', function (err){
               return res.serverError(err);
            })
            .pipe(res);
      });
   },


   logo: function (req, res){
      console.log('Get logo');
      console.log(req.param('id'));
      req.validate({
         id: 'string'
      });

      Event.findOne(req.param('id')).exec(function (err, event){
         console.log('findedOne');
         if (err) return res.negotiate(err);
         if (!event) return res.notFound();

         // Event has no background image uploaded.
         // (should have never have hit this endpoint and used the default image)
         if (!event.logoFd) {
            return res.notFound();
         }

         var SkipperDisk = require('skipper-disk');
         var fileAdapter = SkipperDisk(/* optional opts */);

         // Stream the file down
         fileAdapter.read(event.logoFd)
            .on('error', function (err){
               return res.serverError(err);
            })
            .pipe(res);
      });
   }

};

