/**
* Evento.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    name:'string',
    description:'string',
    logoUrl:'string',
    backgroundcollor: 'string',
    backgroundUrl:'string',
    textcollor:'string',
    uploadMessage:'string',
    cadastreMessage:'string',
    url:'string',
    urlApp:'string'
  }
};

