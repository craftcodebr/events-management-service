/**
 * Created by icastilho on 14/06/15.
 */

function EventService() {

   this.updateBackground = function (id, url, fd) {
      // Save the "fd" and the url where the avatar for a user can be accessed
      return Event.update(id, {

         // Generate a unique URL where the avatar can be downloaded.
         backgroundUrl: url,

         // Grab the first file and use it's `fd` (file descriptor)
         backgroundFd: fd
      });
   },

   this.updateLogo = function (id, url, fd) {
      // Save the "fd" and the url where the avatar for a user can be accessed
      return Event.update(id, {

         // Generate a unique URL where the avatar can be downloaded.
         logoUrl: url,

         // Grab the first file and use it's `fd` (file descriptor)
         logoFd: fd
      });
   }
}

module.exports = new EventService();
